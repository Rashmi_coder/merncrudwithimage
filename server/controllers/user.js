import userModel from "../models/user.js";
class userController {
    static createUser = async (req, res) => {
        const { name, email, age } = req.body;
        const { filename } = req.file;

        try {
            if (name && email && age && filename) {
                const newUser = new userModel({
                    name,
                    email,
                    age,
                    profile: filename
                });
                const new_user = await newUser.save();
                if (new_user) {
                    return res.status(200).json(newUser);
                } else {
                    return res.status(400).json({ message: "something wrong" });
                }
            } else {
                return res.status(400).json({ message: "all field are required" });
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    };

    static getAllUser = async (req, res) => {
        try {
            const allUsers = await userModel.find({});
            if (allUsers) {
                return res.status(200).json(allUsers);
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    };

    static deleteUser = async (req, res) => {
        const { id } = req.params;
        try {
            if (id) {
                const getDeletedData = await userModel.findByIdAndDelete(id);
                return res.status(200).json(getDeletedData);
            } else {
                return res.status(400).json({ message: "Id not found" });
            }
        } catch (error) {
            console.log(error)
            return res.status(400).json(error);
        }
    }

    static getsingleuser = async (req, res) => {
        const { id } = req.params;
        try {
            if (id) {
                const getSingleData = await userModel.findById(id);
                return res.status(200).json(getSingleData);
            }else{
                return res.status(400).json({ message: "Id not found" });
            }
        } catch(error) {
            return res.status(400).json(error);
        }
    }

    static getedituser = async(req,res)=>{
        const { id } = req.params;
        try {
            if (id) {
                const getupdateData = await userModel.findByIdAndUpdate(id, req.body, req.file);
                console.log(getupdateData)
                return res.status(200).json(getupdateData);
            }else{
                return res.status(400).json({ message: "Id not found" });
            }
        } catch(error) {
            return res.status(400).json(error);
        }
    }
}

export default userController;