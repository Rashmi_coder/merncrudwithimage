import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom';
export default function Edit() {
    const {id} = useParams();
    const navigate = useNavigate();
   
    const [input, setInput] = useState({
        name: "",
        email: "",
        age: "",
      });
      // const [profile, setProfile] = useState('');
    useEffect(() => {
        const getAllUsers = async () => {
          const getAllData = await axios.get(`http://localhost:9000/api/v1/users/${id}`);
          console.log("eeeeeee",getAllData)
          setInput(getAllData.data);
        };
        getAllUsers();
      }, [id]);

      const formdata = new FormData();
  formdata.append("name", input.name);
  formdata.append("email", input.email);
  formdata.append("age", input.age);
  // formdata.append("profile", profile);
  const handleEditData = async(e)=>{
e.preventDefault();
await axios.put(`http://localhost:9000/api/v1/users/${id}`, input);
navigate("/");
  }
  return (
    <div className='container'>
    <div className='row'>
      <div className='col-md-12' style={{ backgroundColor: "yellow" }}>   
          <h1 className='text-center'>Update</h1>
        </div>
        <div className='col-md-6'>
          <form onSubmit={handleEditData} encType='multipart/form-data'>
            <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label">Name</label>
              <input type="text" value={input.name} onChange={(e) => setInput({ ...input, [e.target.name]: e.target.value })} name='name' className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
              <input type="email" value={input.email} onChange={(e) => setInput({ ...input, [e.target.name]: e.target.value })} name='email' className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleInputPassword1" className="form-label">Age</label>
              <input type="number" name='age' value={input.age} onChange={(e) => setInput({ ...input, [e.target.name]: e.target.value })} className="form-control" id="exampleInputPassword1" />
            </div>
            {/* <div className="mb-3">
              <label htmlFor="exampleInputPassword1" className="form-label">Profile</label>
              <input type="file" name='profile' onChange={(e) => setProfile(e.target.files[0])} className="form-control" id="exampleInputPassword1" />
            </div> */}
            <button type="submit" className="btn btn-primary">Update</button>
          </form>
        </div>
        </div>
        <button className='btn btn-info mt-2' onClick={()=>navigate('/')}>Go To Home</button>
    </div>
  )
}
