import mongoose from "mongoose";

const connectDB=async()=>{

try {
    const res = await mongoose.connect("mongodb://0.0.0.0:27017/merncrud");
    if(res){
        console.log("connected successfully");
    }  
} catch (error) {
    console.log(error);
}
};

export default connectDB;