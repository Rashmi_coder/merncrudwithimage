
import React, { useState, useEffect } from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom';
export default function Home() {
  
    const [users, setUsers] = useState([]);
    const [render, setRender] = useState(false);
    const [input, setInput] = useState({
      name: "",
      email: "",
      age: "",
    });
    const [profile, setProfile] = useState('');
    useEffect(() => {
      const getAllUsers = async () => {
        const getAllData = await axios.get("http://localhost:9000/api/v1/users");
        console.log("eeeeeee",getAllData)
        setUsers(getAllData.data);
      };
      getAllUsers();
    }, [render]);
  
    const formdata = new FormData();
    formdata.append("name", input.name);
    formdata.append("email", input.email);
    formdata.append("age", input.age);
    formdata.append("profile", profile);
    const handleSubmit = async (e) => {
      e.preventDefault();
      try {
        await axios.post("http://localhost:9000/api/v1/users", formdata);
        setRender(true);
        setInput({
          name: "",
          email: "",
          age: ""
        })
      } catch (error) {
        console.log(error)
      }
    }
  
    const deleteData = async (id) => {
      try {
        await axios.delete(`http://localhost:9000/api/v1/users/${id}`);
        const newUsers = users.filter((item)=>{
          return item._id!==id;
        });
        setUsers(newUsers);
      } catch (err) {
        console.log(err)
      }
    }
  
    
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-md-12' style={{ backgroundColor: "yellow" }}>
            <h1 className='text-center'>Mern CRUD with Image</h1>
          </div>
          <div className='col-md-6'>
            <form onSubmit={handleSubmit} encType='multipart/form-data'>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">Name</label>
                <input type="text" value={input.name} onChange={(e) => setInput({ ...input, [e.target.name]: e.target.value })} name='name' className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                <input type="email" value={input.email} onChange={(e) => setInput({ ...input, [e.target.name]: e.target.value })} name='email' className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">Age</label>
                <input type="number" name='age' value={input.age} onChange={(e) => setInput({ ...input, [e.target.name]: e.target.value })} className="form-control" id="exampleInputPassword1" />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">Profile</label>
                <input type="file" name='profile' onChange={(e) => setProfile(e.target.files[0])} className="form-control" id="exampleInputPassword1" />
              </div>
              <button type="submit" className="btn btn-primary">Submit</button>
            </form>
          </div>
          <div className='col-md-6'>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Age</th>
                  <th scope="col">Profile</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {users && users.map((user, i) => {
                  console.log("kuuu",user)
                  return (
                    <tr key={i}>
                      <td>{user.name}</td>
                      <td>{user.email}</td>
                      <td>{user.age}</td>
                      <td><img className='img img-fluid' src={`http://localhost:9000/${user.profile}`} alt='users' height="50" width="50" /></td>
                      <td>
                        <Link to={`/edit/${user._id}`} className="btn btn-primary ">Edit</Link>
                        <button onClick={(e) => deleteData(user._id)} className='btn btn-danger mr-2'>Delete</button>
                      </td>
                    </tr>
                  )
                })}
  
  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
}
