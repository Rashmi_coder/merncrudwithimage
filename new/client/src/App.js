import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './Home';
import Edit from './Edit';
function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />}></Route>
          <Route path='/edit/:id' element={<Edit />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
