import express from 'express';
import connectDB from './config/db.js';
import cors from 'cors';
import userRoutes from './routes/user.js'
const app = express();

app.use(cors());
app.use(express.json());
connectDB();
app.use(express.static("public/upload"))
app.get('/',(req,res)=>{
    res.send("api is run");
});

//api routes

app.use('/api/v1', userRoutes);

app.listen(9000,()=>{
    console.log('Api is running on http://localhost:9000')
})